<?php

spl_autoload_register(function ($className)
{
	if(file_exists('../classes/'.strtolower($className) . '.class.php')){
		$fichier = '../classes/'.strtolower($className) . '.class.php';
	}else{
		$fichier = '../classes/'.$className . '.class.php';
	}
	if(file_exists($fichier))
		require_once $fichier;
});