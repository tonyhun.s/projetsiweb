<?php

require_once 'autoload.include.php';

$web = new WebPage("BuildBox - Configuration");
$web->setHeader("Configuration");
$web->appendJSUrl("../resources/js/jquery-3.3.1.min.js");
$web->appendJSUrl("../resources/js/parameterModelForm.js");
$web->appendCSSUrl("../resources/css/list.css");
$web->appendCSSUrl("../resources/css/theme.css");

$models = "";

foreach(BuildModel::getAll() as $modelid => $model){
    $parameters = "";
    foreach(ParameterModel::getAllByBuild(["id" => $modelid]) as $parameterid => $parameter){
        $parameters .= <<<HTML
            <li>
                <div>{$parameter["parametermodelname"]}</div>
                <a href="parameters.php?id={$parameterid}">Liste des Paramètres</a>
                <a href="#" class="deleteParameter" data-id="{$parameterid}">Supprimer</a>
            </li>
HTML;
    }
    $models .= <<<HTML
        <div class="listElement">
            <h3>{$modelid} : {$model["buildmodelname"]}</h3>
            <h4>Paramètres</h4>
            <ul class="parameterList">
                {$parameters}
            </ul>
            <form action="../requests/addParameterModel.php" method="POST">
                <label>Ajouter un Paramètre</label><input type="text" name="parametermodelname">
                <button type="submit" name="buildmodel" value="{$modelid}">Ajouter</button>
            </form>
        </div>
HTML;
}

$html = <<<HTML
    <div>
        <a href="buildmodel.php">Créer une Configuration</a>
    </div>
    <div>
        {$models}
    </div>
HTML;

$web->appendContent($html);

echo $web->toHTML();