<?php

require_once 'autoload.include.php';

$id = $_GET["id"] ?? null;

if(!$id){
    header("Location: buildmodels.php");
}

$web = new WebPage("BuildBox - Paramètres");
$web->setHeader(ParameterModel::get(["id" => $id])["parametermodelname"]);
$web->appendJSUrl("../resources/js/jquery-3.3.1.min.js");
$web->appendJSUrl("../resources/js/parameterForm.js");
$web->appendCSSUrl("../resources/css/list.css");
$web->appendCSSUrl("../resources/css/theme.css");

$parameters = "";

foreach(Parameter::getAllByModel(["id" => $id]) as $parameterid => $parameter){
    $parameters .= <<<HTML
    <div class="listElement">
        <div>{$parameter["parametername"]}</div>
        <div>{$parameter["price"]} €</div>
        <a href="#" class="deleteParameter" data-id="{$parameterid}">Supprimer</a>
    </div>
HTML;
}

$html = <<<HTML
    <div>
        <h3>Créer un Paramètre</h3>
        <form action="../requests/addParameter.php" method="POST">
            <label>Nom de Paramètre</label><input type="text" name="parametername" required>
            <label>Prix (€)</label><input type="number" name="price" required>
            <button type="submit" name="parameter_model" value="{$id}">Ajouter</button>
        </form>
    </div>
    <div>
        <h3>Liste des Paramètres</h3>
        {$parameters}
    </div>
HTML;

$web->appendContent($html);

echo $web->toHTML();