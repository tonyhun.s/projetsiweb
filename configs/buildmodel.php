<?php

require_once 'autoload.include.php';

$id = $_GET["id"] ?? null;

$web = new WebPage("BuildBox - Configurations");
$web->setHeader($id ? "Nouvelle Configuration" : "Editer la Configuration");
$web->appendCSSUrl("../resources/css/theme.css");

$html = <<<HTML
    <form action="../requests/addBuildModel.php" method="POST">
        <div>
            <label>Nom de Configuration</label>
            <input type="text" name="buildmodelname">
        </div>
        <button type="submit">Confirmer</button>
    </form>
HTML;

$web->appendContent($html);

echo $web->toHTML();