$(document).ready(function(){
    $(".deleteParameter").click(function(){
        var id = $(this).data("id");
        var node = $(this).parent();
        $.post("../requests/deleteParameterModel.php",{id: id},function(data){
            if(data == 1){
                node.remove();
            }else{
                console.log(data);
            }
        });
    });
});