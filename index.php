<?php

require_once 'autoload.include.php';

$web = new WebPage("BuildBox");
$web->setHeader("Buildbox");
$web->appendCSSUrl("resources/css/theme.css");

$buildmodels = "";
foreach(BuildModel::getAll() as $buildmodelid => $buildmodel){
    $buildmodels .= <<<HTML
    <li>
        <a href="newbuild.php?id={$buildmodelid}">{$buildmodel["buildmodelname"]}</a>
    </li>
HTML;
}

$web->appendContent(<<<HTML
    <ul>
        <li><a href="builds.php">Liste des Builds</a></li>
        <li>Créer un Build
            <ul>{$buildmodels}</ul>
        </li>
        <li><a href="configs/buildmodels.php">Configurations</a></li>
    </ul>
HTML
);

echo $web->toHTML();