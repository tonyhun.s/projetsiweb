<?php

require_once 'autoload.include.php';

class Build{

    public function __construct(){

    }

    public static function get($get) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        SELECT *
        FROM build
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($get);
            return $pdostat->fetch();
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function getParameters($get) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        SELECT parametermodelname, parametername, price
        FROM build, buildparameter, parameter, parametermodel
        WHERE build.id = :id
        AND build.id = buildparameter.build
        AND buildparameter.parameter = parameter.id
        AND parametermodel.id = parameter.parameter_model
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($get);
            return $pdostat->fetchAll();
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function getAll() {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        SELECT build.id, buildname, parametermodelname, parametername, price
        FROM build, buildparameter, parameter, parametermodel
        WHERE build.id = buildparameter.build
        AND buildparameter.parameter = parameter.id
        AND parametermodel.id = parameter.parameter_model
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute();
            return $pdostat->fetchAll(PDO::FETCH_GROUP);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function edit($edit) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        UPDATE build
        SET buildname = :buildname
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($edit);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function add($add) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        INSERT INTO build(user,build_model,buildname)
        VALUES (:user,:buildmodel,:buildname)
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($add);
            return $pdo->lastInsertId();
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function addParameters($build, $parameters){
        $pdo = myPDO::getInstance();
        $args = [];
        $sql = <<<SQL
        INSERT INTO buildparameter(build,parameter)
        VALUES 
SQL;
        $values = [];
        foreach($parameters as $parameter){
            $values[] = "(?,?)";
            $args[] = $build;
            $args[] = $parameter;
        }
        $sql .= implode(",",$values);

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($args);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function delete($delete) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        DELETE FROM build
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($delete);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function deleteParameters($delete){
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        DELETE FROM buildparameter
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($delete);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }
}