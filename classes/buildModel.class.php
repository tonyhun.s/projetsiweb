<?php

require_once 'autoload.include.php';

class BuildModel{

    public function __construct(){

    }

    public static function get($get) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        SELECT *
        FROM buildmodel
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($get);
            return $pdostat->fetch();
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function getAll() {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        SELECT *
        FROM buildmodel
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute();
            return $pdostat->fetchAll(PDO::FETCH_UNIQUE);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function edit($edit) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        UPDATE buildmodel
        SET buildmodelname = :buildmodelname
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($edit);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function add($add) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        INSERT INTO buildmodel(buildmodelname)
        VALUES (:buildmodelname)
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($add);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function delete($delete) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        DELETE FROM buildmodel
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($delete);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }
}