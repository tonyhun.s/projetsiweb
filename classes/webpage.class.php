<?php

class WebPage {
	private $head  = null ;
	private $title = null ;
	private $body  = null ;
	private $header = null;

	public function __construct($title=null) {
		$this->setTitle($title);
	}

	public function setTitle($title) {
		$this->title = $title ;
	}

	public function appendToHead($content) {
		$this->head .= $content ;
	}

	public function appendCss($css) {
		$this->appendToHead(<<<HTML
	<style type='text/css'>
		{$css}
	</style>

HTML
);
	}

	public function appendCssUrl($url) {
		$this->appendToHead(<<<HTML
		<link rel="stylesheet" type="text/css" href="{$url}">
HTML
) ;
	}

	public function appendJs($js) {
		$this->appendToHead(<<<HTML
	<script type='text/javascript'>
		{$js}
	</script>

HTML
) ;
	}

	public function appendJsUrl($url) {
		$this->appendToHead(<<<HTML
		<script type='text/javascript' src='$url'></script>
HTML
) ;
	}

	public function appendContent($content) {
		$this->body .= $content ;
	}

	public function setHeader($titre) {
		$this->header = <<<HTML
	<div><h2>{$titre}</h2></div>
HTML;
	}

	public function toHTML() {
		return <<<HTML
<!doctype html>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>{$this->title}</title>
		{$this->head}
	</head>
	<body>
		{$this->header}
		{$this->body}
	</body>
</html>
HTML;
	}
}