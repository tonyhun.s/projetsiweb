<?php

require_once 'autoload.include.php';

class Parameter{

    public function __construct(){

    }

    public static function get($get) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        SELECT *
        FROM parameter
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($get);
            return $pdostat->fetch();
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function getAll() {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        SELECT *
        FROM parameter
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute();
            return $pdostat->fetchAll(PDO::FETCH_UNIQUE);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function getAllByModel($get) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        SELECT *
        FROM parameter
        WHERE parameter_model = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($get);
            return $pdostat->fetchAll(PDO::FETCH_UNIQUE);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function edit($edit) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        UPDATE parameter
        SET parametername = :parametername
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($edit);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function add($add) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        INSERT INTO parameter(parameter_model,parametername,price)
        VALUES (:parameter_model,:parametername,:price)
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($add);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function delete($delete) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        DELETE FROM parameter
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($delete);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }
}