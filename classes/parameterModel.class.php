<?php

require_once 'autoload.include.php';

class ParameterModel{

    public function __construct(){

    }

    public static function get($get) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        SELECT *
        FROM parametermodel
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($get);
            return $pdostat->fetch();
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function getAllByBuild($get) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        SELECT *
        FROM parametermodel
        WHERE buildmodel = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($get);
            return $pdostat->fetchAll(PDO::FETCH_UNIQUE);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function edit($edit) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        UPDATE parametermodel
        SET parametermodelname = :parametermodelname
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($edit);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function add($add) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        INSERT INTO parametermodel(buildmodel,parametermodelname)
        VALUES (:buildmodel,:parametermodelname)
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($add);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }

    public static function delete($delete) {
        $pdo = myPDO::getInstance();
        $sql = <<<SQL
        DELETE FROM parametermodel
        WHERE id = :id
SQL;

        $pdostat = $pdo->prepare($sql);
        try {
            $pdostat->execute($delete);
        } catch (Exception $e) {
            echo "Erreur : " . $e->getMessage();
        }
    }
}