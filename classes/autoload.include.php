<?php

spl_autoload_register(function ($className)
{
	if(file_exists(strtolower($className) . '.class.php')){
		$fichier = strtolower($className) . '.class.php';
	}else{
		$fichier = $className . '.class.php';
	}
	if(file_exists($fichier))
		require_once $fichier;
});