<?php

require_once 'autoload.include.php';

if(isset($_POST["parameter_model"]) && isset($_POST["parametername"])){
    Parameter::add(["parameter_model" => $_POST["parameter_model"] , "parametername" => $_POST["parametername"], "price" => $_POST["price"]]);
    header("Location: ../configs/parameters.php?id=".$_POST["parameter_model"]);
}else{
    header("Location: ../configs/buildmodels.php");
}