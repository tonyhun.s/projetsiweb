<?php

require_once 'autoload.include.php';

$id = $_GET["id"] ?? null;

if(!$id){
    header("Location: index.php");
}

$buildmodel = BuildModel::get(["id" => $id]);

$web = new WebPage("BuildBox - Création de ".$buildmodel["buildmodelname"]);
$web->setHeader("Nouvelle Configuration - ".$buildmodel["buildmodelname"]);
$web->appendCSSUrl("../resources/css/theme.css");

$buildparameters = "";

foreach(ParameterModel::getAllByBuild(["id" => $id]) as $parametermodelid => $parametermodel){
    $parametervalues = "";

    foreach(Parameter::getAllByModel(["id" => $parametermodelid]) as $parameterid => $parameter){
        $parametervalues .= <<<HTML
        <option value="{$parameterid}">{$parameter["parametername"]} - {$parameter["price"]} €</option>
HTML;
    }

    $buildparameters .= <<<HTML
    <label>{$parametermodel["parametermodelname"]}</label>
    <select name="parameter[{$parametermodelid}]">
         {$parametervalues}
    </select>
HTML;
}

$html = <<<HTML
    <form action="requests/addBuild.php" method="POST">
        <div>
            <label>Nom de Configuration</label>
            <input type="text" name="buildname" required>
        </div>
        <div>
            {$buildparameters}
        </div>
        <button type="submit" name="buildmodel" value="{$id}">Confirmer</button>
    </form>
HTML;

$web->appendContent($html);

echo $web->toHTML();