<?php

require_once 'autoload.include.php';

$web = new WebPage("BuildBox - Builds");
$web->setHeader("Builds");
$web->appendCSSUrl("resources/css/theme.css");
$web->appendCSSUrl("resources/css/list.css");

$html = "";

foreach(Build::getAll() as $buildid => $build){
    $parameters = "";
    $price = 0;

    foreach($build as $buildparameters){
        $parameters .= <<<HTML
        <li class="listElement">
            <div>{$buildparameters["parametermodelname"]}</div>
            <div>{$buildparameters["parametername"]} - {$buildparameters["price"]} €</div>
        </li>
HTML;
        $price += $buildparameters["price"];
    }

    $html .= <<<HTML
    <div class="listElement">
        <h3>{$buildid} : {$build[0]["buildname"]}</h3>
        <ul>
            {$parameters}
        </ul>
        <div>Prix Total : {$price} €</div>
    </div>
HTML;
}

$web->appendContent($html);

echo $web->toHTML();