Projet BuildBox,

Projet suivant le même principe que les constructeurs de pc, comme Matériel.net, monPCsurmesure, etc.
Il sera possible de créer d'autres modèles de constructions autre que des ordinateurs.

Je n'ai pas réussi à faire fonctionner docker sur mon pc, ça sera donc un projet php avec juste les sources.

tables.sql pour voir le script de création de la bd
et BuildBoxDiagramme.png pour avoir un aperçu de la table.

Fonctionnement :

    Il faudra tout d'abord créer un type de build dans l'application.
    Dans Configurations, une liste des types de builds apparaîtra.
    On pourra y créer ainsi un modèle de configuration avec chaque paramètre nécéssaire au type de construction voulue.
    
    Après la création d'un modèle, il faudra ajouter des paramètres à configurer pour pouvoir moduler chacun des builds à créer.
    
    Quand chaque paramètre aura été créé, on pourra créer de nouveaux builds et ainsi afficher une liste de builds qui pourra être notée par d'autres utilisateurs.
    
Pas fini :

    Un bon Css
    
    Gestion des utilisateurs
    
    Notation de builds